import json
import string
# import pdb
from sklearn import svm, preprocessing
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import tree
from sklearn_porter import Porter
import glob
from sklearn.externals import joblib

DATA_LOCATION = '/Users/kuzi/Projects/tower/research/data'
RUNS_LOCATION = DATA_LOCATION
BRAINS_LOCATION = DATA_LOCATION + '/brains'
BEST_RUNS_LOCATION = DATA_LOCATION + '/best-runs'

RUNS_PATTERN = RUNS_LOCATION + '/*.json'

FILES_TO_READ = glob.glob(RUNS_PATTERN)

LIMIT_MOVES_TO = 6
MAX_MOVES_TO_PROCESS = 6
LIMIT_GAMES_PROCESSED = 5000
INCLUDE_TIME = True
SCALE_DATA = True
FILL_MISSING_DATA_WITH_ZERO = False

MOVE_ARRAY_LENGTH = 4

if INCLUDE_TIME:
    MOVE_ARRAY_LENGTH += 1

N_FEATURES = (LIMIT_MOVES_TO | MAX_MOVES_TO_PROCESS) * MOVE_ARRAY_LENGTH

def fill_with_zeros_to_n_features(array_to_fill):
    """
    Fills with array zeros up to N_FEATURES length
    """
    if len(array_to_fill) < N_FEATURES:
        i = len(array_to_fill)
        while (i < N_FEATURES):
            array_to_fill = np.append(array_to_fill, [0])
            i += 1

    if (len(array_to_fill) > N_FEATURES):
        array_to_fill = array_to_fill[:N_FEATURES]

    return array_to_fill

def milliseconds_to_seconds(milliseconds):
    """
    Convert milliseconds to seconds
    """
    return round(milliseconds / 1000)

def parse_json_game(game_play_data, include_time):
    """
    Parses JSON object and returns 2D representation
    of moves (INPUT) and targets (OUTPUT)
    """
    moves_data = game_play_data['moves']

    moves_2d = []

    moves_processed = 0

    if LIMIT_MOVES_TO:
      # Safety guard for ensuring constant features length
      if (len(moves_data) < LIMIT_MOVES_TO):
          return

    for move in moves_data:
        if LIMIT_MOVES_TO:
          if (moves_processed == LIMIT_MOVES_TO):
              break

        move_array = []

        move_array.append(move['action'])
        move_array.append(move['towerType'])
        move_array.append(move['position']['x'])
        move_array.append(move['position']['y'])

        if (include_time):
            move_array.append(milliseconds_to_seconds(move['time']))

        moves_2d.append(move_array)

        moves_processed += 1

    score = game_play_data['score']

    # score.append(milliseconds_to_seconds(game_play_data['time']))

    # Merge individual move arrays together so moves is represented as one array
    moves_2d = np.concatenate(moves_2d)

    if FILL_MISSING_DATA_WITH_ZERO:
        moves_2d = fill_with_zeros_to_n_features(moves_2d)

    return {
        'moves': moves_2d,
        'result': score
    }

GAMES_HISTORY_DATA = []

for file_name in FILES_TO_READ:
    with open(file_name) as data_file:
        JSON_DATA = json.load(data_file)
        GAMES_HISTORY_DATA.append(JSON_DATA['gamesHistory'])

GAMES_HISTORY_DATA = np.concatenate(GAMES_HISTORY_DATA)

DATA = []
TARGET = []

GAMES_PROCESSED = 0

for GAMEPLAY_HISTORIC_DATA in GAMES_HISTORY_DATA:
    if (GAMES_PROCESSED == LIMIT_GAMES_PROCESSED):
        break

    parsed_data = parse_json_game(GAMEPLAY_HISTORIC_DATA, INCLUDE_TIME)

    if (parsed_data):
      DATA.append(parsed_data['moves'])
      TARGET.append(parsed_data['result'])

      GAMES_PROCESSED += 1

CLASSIFIER = svm.SVC(gamma=0.001, C=100)

# print TARGET
# print DATA

SCALER = preprocessing.StandardScaler().fit(DATA)

if SCALE_DATA:
  DATA = SCALER.transform(DATA)


# print SCALER.transform(DATA)

CLASSIFIER.fit(DATA, TARGET)

TEST_DATA = {
  'DATA': [
    # 145
    [0, 0, 9, 20, 0, 0, 0, 6, 6, 0, 0, 0, 18, 9, 14, 0, 0, 18, 5, 32, 0, 1, 16, 17, 42, 0, 1, 18, 8, 51], # 145
    # 145
    [0, 1, 13, 18, 0, 0, 0, 9, 12, 0, 0, 0, 9, 13, 0, 0, 0, 9, 3, 26, 0, 1, 19, 19, 38, 0, 1, 8, 8, 46],
    # 145
    [0, 0, 17, 15, 0, 0, 0, 17, 10, 0, 0, 0, 13, 2, 20, 0, 1, 15, 1, 35, 0, 1, 4, 11, 44, 0, 1, 19, 6, 53],
    # 145
    [0, 0, 13, 15, 0, 0, 0, 9, 4, 0, 0, 1, 6, 13, 0, 0, 1, 20, 16, 20, 0, 1, 9, 14, 32, 0, 1, 14, 16, 42],

    # -14
    [0, 0, 21, 16, 0, 0, 1, 16, 15, 0, 0, 0, 2, 3, 0, 0, 1, 17, 14, 28, 0, 1, 5, 9, 39, 0, 1, 6, 4, 49],
    # 31
    [0, 0, 8, 19, 0, 0, 0, 9, 16, 0, 0, 0, 5, 18, 14, 0, 1, 13, 1, 26, 0, 1, 6, 11, 36, 0, 1, 14, 18, 46],

    # 145
    [0, 0, 2, 2, 1, 0, 0, 2, 2, 3, 0, 0, 2, 2, 13, 0, 1, 2, 2, 31, 0, 1, 2, 2, 41, 0, 1, 2, 2, 53]
  ],
  'TARGET': [
    145,
    145,
    145,
    145,
    -14,
    31,
    145
  ]
}

# TEST_DATA = {
#   'DATA': [
#     [0, 0, 13, 15, 0, 0, 0, 9, 4, 0, 0, 1, 6, 13, 0, 0, 1, 20, 16, 20, 0, 1, 9, 14, 32, 0, 1, 14, 16, 42, 0, 1, 14, 8, 51]
#   ],
#   'TARGET': [
#     145
#   ]
# }

GAMES_TO_TEST = TEST_DATA['DATA']

CURRENT_GAME = 0

for MOVES_ARRAY in GAMES_TO_TEST:
    if FILL_MISSING_DATA_WITH_ZERO:
        MOVES_ARRAY = fill_with_zeros_to_n_features(MOVES_ARRAY)

    GAMES_TO_TEST[CURRENT_GAME] = MOVES_ARRAY[:N_FEATURES]
    CURRENT_GAME += 1

if SCALE_DATA:
    GAMES_TO_TEST = SCALER.transform(GAMES_TO_TEST)

PREDICTED_TARGET = CLASSIFIER.predict(GAMES_TO_TEST)

print PREDICTED_TARGET

print 'SHOULD BE'

print TEST_DATA['TARGET']

TOTAL_MISTAKE = 0

for i in xrange(0, len(PREDICTED_TARGET)):
    TOTAL_MISTAKE += np.absolute(TEST_DATA['TARGET'][i] - PREDICTED_TARGET[i])

print 'TOTAL MISTAKE: ' + str(TOTAL_MISTAKE)

print 'AVERAGE MISTAKE: ' + str(TOTAL_MISTAKE / len(PREDICTED_TARGET))

# JSONdata = [
#     # [ ACTION, TOWER_TYPE, POS_X, POS_Y, TIME]
#     [0, 0, 4, 7, 0]
# ]

# target = [
#     # [SCORE, TIME]
#     [145, 53199]
# ]

# classifier.fit(JSONdata, target)



# r = classifier.predict(zeroImage)

# print(r)


# pdb.set_trace()

print 'PROCESSED GAMES: '
print GAMES_PROCESSED

porter = Porter(CLASSIFIER, language='js')
output = porter.export()
# print(output)

file = open(BRAINS_LOCATION + '/brain-' + str(GAMES_PROCESSED) + '.js', 'w')
file.write(output)
file.close()

scaler_filename = BRAINS_LOCATION + '/scaler-brain-' + str(GAMES_PROCESSED) + '.js'
joblib.dump(SCALER, scaler_filename)
