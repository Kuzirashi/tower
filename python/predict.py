from sklearn.externals import joblib
from argparse import ArgumentParser

parser = ArgumentParser()

# Add more options if you like
parser.add_argument("-i", "--input", dest="input")
parser.add_argument("-b", "--brain", dest="brain")

args = parser.parse_args()

# print(args)

TEST_DATA= [
  # [0, 0, 9, 20, 0, 0, 0, 6, 6, 0, 0, 0, 18, 9, 14, 0, 0, 18, 5, 32, 0, 1, 16, 17, 42, 0, 1, 18, 8, 51]
]

TEST_DATA.append(args.input.split(' '))

DATA_LOCATION = '/Users/kuzi/Projects/tower/research/data'
RUNS_LOCATION = DATA_LOCATION
BRAINS_LOCATION = DATA_LOCATION + '/brains'

SCALER_FILENAME = BRAINS_LOCATION + '/scaler-brain-' + str(args.brain) + '.js'

SCALER = joblib.load(SCALER_FILENAME)

TRANSFORMED_DATA = SCALER.transform(TEST_DATA)

STRING = ''

for DATA in TRANSFORMED_DATA[0]:
  STRING += str(DATA) + ' '

print STRING
