module.exports = function (api) {
    return {
      plugins: [
        
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        '@babel/plugin-proposal-class-properties',
      ],
      presets: ['@babel/preset-env', '@babel/preset-typescript'],
    };
  };