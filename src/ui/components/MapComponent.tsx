import React, { Component } from 'react';
import Game, { GameEventType } from '../../utils/classes/game';
import GameMap from '../../utils/classes/map';
import { TowerType } from '../../utils/classes/tower';
import { TileComponent } from './tile-component/component';
import { TowerComponent } from './TowerComponent';
import { MonsterComponent } from './MonsterComponent';
import { TowerProjectile } from './TowerProjectile';
import { HighlightPositions } from './HighlightPositions';
import { ThreadedPlayer } from '../../utils/classes/ai';

export class MapComponent extends Component<{ game: Game }> {
  get game() {
    return this.props.game;
  }
  
  gameMap : GameMap

  gold : number
  score : number
  lives : number
  towers = []
  entities = []
  projectiles = []
  autoMakeActions = true

  constructor(owner, args) {
    super(owner, args);

    this.start = this.start.bind(this);
    this.startGameLoop = this.startGameLoop.bind(this);
    this.buildTower = this.buildTower.bind(this);
    this.speedUpMonsters = this.speedUpMonsters.bind(this);
    this.changeAutoMakeActions = this.changeAutoMakeActions.bind(this);
  }

  highlightPositions = []

  changeAutoMakeActions(event : React.ChangeEvent<HTMLInputElement>) {
    this.autoMakeActions = !this.autoMakeActions;

    this.game.autoMakeActions = this.autoMakeActions;
  }

  start() {
    this.game.start();
  }

  stop() {
    this.game.stop();
  }

  startGameLoop() {
    this.game.startGameLoop();
  }

  destroyTowers() {
    this.game.destroyTowers();
  }

  spawnRogue() {
    this.game.spawnMonster();
  }

  gridVisible : Boolean = false

  toggleGrid() {
    this.gridVisible = !this.gridVisible;
  }

  highlightsVisible : Boolean = false

  toggleHighlights() {
    this.highlightPositions = this.game.map.road;
    this.highlightsVisible = !this.highlightsVisible;
  }

  buildTower(type : string) {
    switch (type) {
      case 'ice':
        this.game.buildTower(null, false, TowerType.Ice);
        break;
      case 'orc':
        this.game.buildTower(null, false, TowerType.Orc);
        break;
    }
  }

  speedUpMonsters() {
    this.game.speedUpMonsters();
  }

  leakInstance() {
    console.log('MapComponent:', this);
  }

  componentDidMount() {
    this.game.sceneReady();

    this.game.addObserver(GameEventType.GoldChanged, gold => {
      this.gold = gold;
    });

    this.game.addObserver(GameEventType.ScoreChanged, score => {
      this.score = score;
    });

    this.game.addObserver(GameEventType.LivesChanged, lives => {
      this.lives = this.game.lives;
    });

    this.game.addObserver(GameEventType.TowerPlaced, () => {
      this.towers = this.game.towers;
    });

    this.game.addObserver(GameEventType.GameReady, () => {
      console.log('GameReady');
      this.gold = this.game.gold;
      this.score = this.game.score;
      this.towers = this.game.towers;
      this.lives = this.game.lives;
    });

    this.game.addObserver(GameEventType.MonsterDied, () => {
      this.entities = this.game.entities;
    });

    this.game.addObserver(GameEventType.MonsterSpawned, () => {
      // console.log('MonsterSpawned', this.game.entities);
      this.entities = this.game.entities;
    });

    this.game.addObserver(GameEventType.ProjectilesUpdated, () => {
      this.projectiles = this.game.projectiles;
    });

    this.game.addObserver(GameEventType.End, () => {
      this.towers = [];
      this.entities = [];
      this.projectiles = [];
      this.game.lighting.clear();
    });

    window['threadedPlayer'] = new ThreadedPlayer();    
  }

  render() {
    
// {{!--<button onclick={{action spawnRogue}}>Spawn Rogue</button>--}}
// {{!--<button onclick={{action toggleGrid}}>Toggle Grid</button>--}}
// {{!--<button onclick={{action toggleHighlights}} >Toggle highlights</button>--}}

    return <div className="game-ui">
    <div className="stage-wrapper">
      <img src="public/img/tiles/map2.png" />

      <div className="tiles">
        {this.game.map.tiles.map((tile, index) => <TileComponent model={tile} key={`${index}${tile.position.x}${tile.position.y}`} />)}
      </div>

      <div className="entities">
        {this.game.entities.map((entity, index) => <MonsterComponent model={entity} key={entity.id} />)}
    </div>

    <div className="others">
      {this.highlightsVisible && 
        <HighlightPositions positions={this.highlightPositions} />
      }
    </div>

      <div className="towers">
      {this.game.towers.map((tower, index) => <TowerComponent model={tower} key={tower.id} />)}
      </div>

      <div className="projectiles">
      {this.game.projectiles.map((projectile, index) => <TowerProjectile model={projectile} key={projectile.id} />)}
    </div>

      <canvas id="canvas" width="735" height="740"></canvas>
    </div>

     <div className="interface">
    <div className="tower-icon" onClick={() => this.buildTower('ice')}>
      <img src="public/img/towers/ice/level 1/icon.png"/>
    </div>
    <div className="tower-icon" onClick={() => this.buildTower('orc')}>
      <img src="public/img/towers/orc/level 1/icon.png"/>
    </div>
    <hr/>
    <div className="gold"><img src="public/img/interface/iconGold.png"/> {this.gold} </div>
    <div className="score"><img src="public/img/interface/iconScore.png"/> {this.score} </div>
    <hr/>
    <div className="lives"><img src="public/img/interface/iconLives.png"/> {this.lives} </div>
    <hr/>
    <button onClick={this.start}>Start</button>
    <button onClick={this.stop}>Stop</button>
    <hr/>

    <input type='checkbox' checked={this.autoMakeActions} onChange={this.changeAutoMakeActions} />

    Let computer play

    <hr/>

    <button onClick={this.startGameLoop}>Start Processing</button>
    <button onClick={this.speedUpMonsters}>Speed up monsters</button>
  </div> 
  </div>
  }
};
