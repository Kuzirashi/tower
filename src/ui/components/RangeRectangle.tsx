import React from 'react';
import Position from '../../utils/core/position';
import config from '../../utils/config/game-config';

interface Props {
  start: Position;
  range: number;
}

export function RangeRectangle(props: Props) {
  const { start, range } = props;

  const { column, row } = start.toGridPosition();

  const rowStart = Math.max(row - range, 1);
  const rowEnd = Math.min(row + range + 1, config.MAP.TILES_AMOUNT_Y);

  const columnStart = Math.max(column - range, 1);
  const columnEnd = Math.min(column + range + 1, config.MAP.TILES_AMOUNT_X);


  return <div className="rectangle" style={{
    gridRowStart: rowStart,
    gridRowEnd: rowEnd,
    gridColumnStart: columnStart,
    gridColumnEnd: columnEnd
  }}></div>
};
