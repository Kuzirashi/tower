import React from 'react';
import { HighlightSinglePosition } from './HighlightSinglePosition';
import MapPosition from '../../utils/core/position';

interface Props {
    positions: MapPosition[];
}

export function HighlightPositions(props: Props) {
    return <>
    {props.positions.map(position => <HighlightSinglePosition position={position} key={position.id} />)}
  
</>
}