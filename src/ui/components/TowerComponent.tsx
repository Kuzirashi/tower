import React, { Component } from 'react';
import MapPosition, { getOffsetInPixels } from '../../utils/core/position';
import config from '../../utils/config/game-config';
import Tower, { TowerState, TowerEventType } from '../../utils/classes/tower';
import { StatusBar } from './StatusBar';
import { RangeRectangle } from './RangeRectangle';

interface Args {
  model: Tower;
}

export class TowerComponent extends Component<Args> {
  model = null

  position : MapPosition
  rangeRectangleVisible : boolean = false
  menuOpened : boolean = false
  level : number = 1
  constructionProgress : number = 0
  isBuilt : Boolean = false
  towerState : TowerState

  constructor(props, context) {
    super(props, context);

    const tower : Tower = this.props.model;

    this.position = tower.position;

    if (tower.state === TowerState.NotPlaced) {
      this.rangeRectangleVisible = true;
    }

    tower.addObserver(TowerEventType.Upgraded, () => {
      this.level = tower.level;
    });

    tower.addObserver(TowerEventType.ConstructionProgress, (progress) => {
      this.constructionProgress = progress;
      this.towerState = tower.state;
    });

    tower.addObserver(TowerEventType.Built, () => {
      this.isBuilt = tower.isBuilt;
      this.towerState = tower.state;
    });

    this.openMenu = this.openMenu.bind(this);
    this.showRangeRectangle = this.showRangeRectangle.bind(this);
    this.hideRangeRectangle = this.hideRangeRectangle.bind(this);
    this.upgrade = this.upgrade.bind(this);
  }

  get style() {
    const { row, column } = this.position.toGridPosition();

    return {
      gridColumn: column,
      gridRow: row,
      background: `url('${(this.props.model).spriteURL}')`
    };
  }

  get showConstructionProgress() {
    return this.towerState === TowerState.Construction;
  }

  showRangeRectangle() {
    if (!this.rangeRectangleVisible) {
      this.rangeRectangleVisible = true;
    }
  }

  hideRangeRectangle() {
    const tower : Tower = this.props.model;

    if (this.rangeRectangleVisible && tower.state !== TowerState.NotPlaced) {
      this.rangeRectangleVisible = false;
    }
  }

  upgrade() {
    const tower : Tower = this.props.model;

    tower.upgrade();
  }

  openMenu() {
    const tower : Tower = this.props.model;

    if (tower.state === TowerState.Built) {
      this.menuOpened = !this.menuOpened;
    }
  }

  componentDidMount() {
    const tower : Tower = this.props.model;

    if (tower.state !== TowerState.NotPlaced) {
      return;
    }

    const mouseMoveListener = event => {
      const mapOffset = getOffsetInPixels((document.getElementsByClassName('stage-wrapper')[0]) as HTMLElement);

      const cursorPositionOnMapInPixelsX = event.clientX - mapOffset.x;
      const cursorPositionOnMapInPixelsY = event.clientY - mapOffset.y;

      const X = Math.min(Math.max(Math.floor(cursorPositionOnMapInPixelsX / config.MAP.TILE_PIXEL_SIZE), 0), config.MAP.TILES_AMOUNT_X - 1);
      const Y = config.MAP.TILES_AMOUNT_Y - 1 - Math.floor(cursorPositionOnMapInPixelsY / config.MAP.TILE_PIXEL_SIZE);

      this.props.model.position = new MapPosition(X, Y);
      this.position = this.props.model.position;
    };

    document.addEventListener('mousemove', mouseMoveListener, false);

    const clickListener = () => {
      tower.placeOnMap();
      this.rangeRectangleVisible = false;
      this.state = tower.state;

      document.removeEventListener('mousemove', mouseMoveListener);
      document.removeEventListener('click', clickListener);
    };

    document.addEventListener('click', clickListener);
  }

  render() {
    return (<>
    <div className="tower" style={this.style} onClick={this.openMenu} onMouseOver={this.showRangeRectangle} onMouseOut={this.hideRangeRectangle}>
  {this.showConstructionProgress &&
    <StatusBar current={this.constructionProgress} max={100} background='#525252' color='white'/>
  }
</div>

{this.rangeRectangleVisible &&
  <RangeRectangle start={this.position} range={this.props.model.range} />
}

{this.menuOpened &&
  <div className="tower-menu">
    <img src="public/img/interface/sell.png" />
    <img src="public/img/interface/upgrade.png" onClick={this.upgrade}/>
  </div>
}
</>)
  }
};
