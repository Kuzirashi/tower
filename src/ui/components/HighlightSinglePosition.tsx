import React from 'react';
import MapPosition, { GridPosition } from '../../utils/core/position';

interface Props {
  position: MapPosition
};

export function HighlightSinglePosition(props: Props) {

    const { row, column } = props.position.toGridPosition();



  return <div className="highlight-single-position" style={{gridColumn: column,
  gridRow: row}}></div>
};
