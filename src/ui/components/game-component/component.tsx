import React, { Component } from 'react';
import { MapComponent } from '../MapComponent';
import Game from '../../../utils/classes/game';

export function GameComponent(props: { game: Game } ) {
return <div>
<MapComponent game={props.game} />
</div>
}
