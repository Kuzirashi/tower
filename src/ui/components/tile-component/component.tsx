import React from 'react';
import { Tile } from '../../../utils/classes/map';

interface Props {
    model: Tile;
}

export function TileComponent(props: Props) {
    const { model } = props;

    return <div className={`tile ${model.typeClass}`}>
    <div className="coords">X: {model.position.x}}<br/>Y: {model.position.y}</div>
</div>
}
