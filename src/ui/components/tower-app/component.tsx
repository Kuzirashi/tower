import React, { Component } from 'react';
import { GameComponent } from '../game-component/component';
import Game from '../../../utils/classes/game';

export function TowerApplication(props: { game: Game }) {
  return <div>
    <GameComponent game={props.game} />
</div>
}
