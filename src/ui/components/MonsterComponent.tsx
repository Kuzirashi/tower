import React, { Component } from 'react';
import Monster, { MonsterEventType } from '../../utils/entities/monster';
import MapPosition, { GridPosition } from '../../utils/core/position';
import { StatusBar } from './StatusBar';

let indexCounter : number = 1000;

export class MonsterComponent extends Component<{
  model: Monster
}> {
  monster : Monster
  margin : any = {}
  index : number = 0
  health : number
  maxHealth : number
  spriteURL : string
  isDead : Boolean

  private _timeSinceLastUpdate : number

  constructor(props, context) {
    super(props, context);

    this.monster = props.model;

    this.health = this.monster.health;

    this.maxHealth = this.monster.maxHealth;

    this.margin = {
      top: this.monster.margin.top,
      left: this.monster.margin.left
    };

    this.monster.addObserver(MonsterEventType.POSITION_MARGIN_CHANGED, newMargin => {
      const { health, spriteURL, isDead } = this.monster;

      if (this.margin.top !== newMargin.top || this.margin.left !== newMargin.left) {
        this.margin = {
          top: newMargin.top,
          left: newMargin.left
        };
      }

      if (this.health !== health) {
        this.health = health;
      }

      if (this.spriteURL !== spriteURL) {
        this.spriteURL = spriteURL;
      }

      if (this.isDead !== isDead) {
        this.isDead = isDead;
      }
    });

    if (indexCounter <= 1) {
      indexCounter = 1000;
    }

    this.index = indexCounter--;

    this.takeHealth = this.takeHealth.bind(this);
  }

  takeHealth() {
    this.monster.takeDamage(30);
  }
  
  render() {
    const { model } = this.props;
    const { column, row } = model.position.toGridPosition();

   
    return <div className="monster" style={{
      gridColumn: column,
      gridRow: row,
      marginTop: `${this.monster.margin.top}px`,
      marginLeft: `${this.monster.margin.left}px`
    }} onClick={this.takeHealth}>
      {!this.isDead && <StatusBar current={this.health} max={this.maxHealth} />}

    <img src={this.spriteURL} />
  </div>
  }
};


