import React from 'react';
import Projectile from '../../utils/classes/projectile';

export function TowerProjectile(props: { model: Projectile }) {
  const { model } = props;
  return <div className="tower-projectile" style={{
    transform: `
      translateX(${model.position.x}px)
      translateY(${model.position.y}px)
      rotate(${model.rotation}rad)`
  }}><img src={model.spriteURL} /></div>
}
