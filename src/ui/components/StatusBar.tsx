import React from 'react';

interface Props {
  current: number;
  max: number;
  color?: string;
  background?: string;
}

export function StatusBar(props: Props) {
  const { background, current, max, color } = props;

  return <div className="status-bar" style={{
    background
  }}>
  <div className="status-fulfillment" style={
    {
      width: `${(current / max) * 100}%`,
      background: color
    }
  }></div>
</div>
}
