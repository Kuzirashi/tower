import { render } from 'react-dom';
import React from 'react';
import { TowerApplication } from './ui/components/tower-app/component';
import './ui/styles/app.scss';
import Game, { GameEventType } from './utils/classes/game';
import UserAI from './utils/classes/user-ai';

const ai = new UserAI();
const game = new Game(true, true, ai);

function renderReact() {
    render(
        <TowerApplication game={game} />,
        document.getElementById('app')
    );
}

function startRenderReact(game?: Game) {
    renderReact();
    game.addObserver(GameEventType.RequestRender, renderReact);
}

startRenderReact(game);

window['ai'] = ai;
