import MapPosition from '../core/position';
import { Tile, TileType } from './map';

export default class Pathfinding {
  public startPosition : MapPosition

  public movableTiles : Array<Tile> = []
  public movablePositions : Array<MapPosition> = []

  constructor(startPosition : MapPosition, tiles : Array<Tile>) {
    this.startPosition = startPosition;
    this.movableTiles = tiles.filter(tile => tile.type === TileType.Road);
    this.movablePositions = this.movableTiles.map(tile => tile.position);
  }

  findPathTo(destination : MapPosition) : Array<MapPosition> {
    const path = [];

    let openFields : Array<MapPosition> = [];
    let closedFields : Array<MapPosition> = [];

    openFields = this.movablePositions.filter(position => !position.matches(this.startPosition));

    let currentWorkingPosition = this.startPosition;

    while (openFields.length > 0 && currentWorkingPosition) {
      const siblingPositions = openFields.filter(position => position.isStraightNeighbourOf(currentWorkingPosition));

      const nextMove = siblingPositions[0];

      if (!nextMove) {
        break;
      }

      path.push(nextMove);

      openFields = openFields.filter(position => !position.matches(nextMove));

      closedFields.push(currentWorkingPosition);

      currentWorkingPosition = nextMove;
    }

    return path;
  }
}
