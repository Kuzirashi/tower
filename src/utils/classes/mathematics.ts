export default class Mathematics {
  randomIntFromInterval(min : number, max : number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}

export const mathematicsInstance : Mathematics = new Mathematics();
