import Game from "./game";

export interface IUserAI {
    attachToGame(game: Game);
    update(simulationTimestep: number);
}