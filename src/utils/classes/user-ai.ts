import MapPosition from '../core/position';
import Game, { GameEventType } from './game';
import CONFIG from '../config/game-config';
import { getEnumValues } from '../helpers/typescript';
import { ThreadedPlayer } from './ai';
import { GamePlay, Move, IMove, DEFAULT_MAX_SIMULATIONS } from './gamePrimitives';

const CONSOLE_TIME_ID = '[USER-AI][SIMULATIONS]';

export enum UserAIEventType {
  FinishedSimulations
}

export default class UserAI {
  moves : Move[] = []
  simulationsRan : number = 0

  maxSimulations : number = DEFAULT_MAX_SIMULATIONS

  gamesHistory : GamePlay[] = []

  public currentGame : Game

  private _observers = {}

  constructor(maxSimulations? : number) {
    if (maxSimulations) {
      this.maxSimulations = maxSimulations;
    }

    const eventTypes = getEnumValues(UserAIEventType);

    eventTypes.forEach(event => {
      this._observers[event] = [];
    });
  }

  async start(maxSimulations? : number, batchSize?: number) {
    if (maxSimulations) {
      this.maxSimulations = maxSimulations;
    }

    console.time(CONSOLE_TIME_ID);

    const player = new ThreadedPlayer();

    this.gamesHistory.push(...await player.play(maxSimulations, batchSize));

    this.finishedSimulations();
  }

  saveGameFromJSON(input) {
    const parsed: { moves: IMove[], score: number, time: number } = JSON.parse(input);

    const moves : Move[] = [];

    parsed.moves.forEach(move => {
      let position = null;

      if (move.position) {
        position = new MapPosition(move.position.x, move.position.y);
      }

      const newMove = new Move(move.action, position, move.towerType, move.time);

      moves.push(newMove);
    });

    const gameplay = new GamePlay(parsed.score, moves, parsed.time);

    this._bestGamePlayed = gameplay;

    return gameplay;
  }

  private _executeMove(moveToBeExecuted : Move) : void {
    moveToBeExecuted.execute(this.currentGame);

    this._movesToExecute = this._movesToExecute.filter(move => move !== moveToBeExecuted);

    // console.log('after: ', this._movesToExecute.length);
  }

  getGamesSorted() {
    return this.gamesHistory.sort((a, b) => {
      if (b.score > a.score) {
        return 1;
      }

      if (b.score < a.score) {
        return -1;
      }

      if (b.score === a.score) {
        if (b.time < a.time) {
          return 1;
        }

        if (b.time > a.time) {
          return -1;
        }
      }

      return 0;
    });
  }

  private _bestGamePlayed : GamePlay

  getBestGamePlayed() : GamePlay {
    const gamesSortedByScore = this.getGamesSorted();

    if (!this._bestGamePlayed) {
      this._bestGamePlayed = gamesSortedByScore[0];
    }

    return this._bestGamePlayed;
  }

  public getSerializedGamesHistory() {
    const data = {
      gamesHistory: {}
    };

    data.gamesHistory = this.gamesHistory;

    return JSON.stringify(data);
  }

  public addObserver(type : UserAIEventType, callback) : void {
    if (!this._observers[type]) {
      throw new Error('There is no such event type to subscribe. Type: `' + type + '`');
    }

    this._observers[type].push(callback);
  }

  finishedSimulations() : void {
    const bestGame = this.getBestGamePlayed();

    if (bestGame) {
      console.log(`[USER-AI] [FINISHED SIMULATIONS] [CHOOSING ONE WITH ${bestGame.score} SCORE][TIME: ${bestGame.getHumanReadableTime()}]`);

    } else {
      console.log(`[USER-AI] [FINISHED SIMULATIONS] [NO BEST GAME]`);
    }

    console.timeEnd(CONSOLE_TIME_ID);

    this._fireObservers(UserAIEventType.FinishedSimulations);
  }

  hasAvailableSimulations() : Boolean {
    return this.simulationsRan < this.maxSimulations;
  }

  public positionsRoadIntersectsMap : Array<any> = null
  public bestPositionsToPlaceTowers = [];

  getBestPositionsToPlaceTowers() : Array<any> {
    if (this.currentGame.map && !this.positionsRoadIntersectsMap) {
      this.positionsRoadIntersectsMap = this.currentGame.map.getPositionsIntersectingRoadMap();

      this.bestPositionsToPlaceTowers = this.positionsRoadIntersectsMap.slice(0, 100);
    }

    return this.bestPositionsToPlaceTowers;
  }

  public isReplaying() : Boolean {
    return Boolean(this._movesToExecute.length > 0);
  }

  update(timeToBeSimulated : number) : void {
    // console.log('update AI');
    if (this.isReplaying()) {
      // console.log('isReplaying');
      const nextMove = this.getNextMove();

      // console.log('next time', nextMove && nextMove.time, ' vs ', this.currentGame.totalGameTime);
      if (nextMove && this.currentGame.totalGameTime >= nextMove.time) {
        // console.log('EXECUTE: ', nextMove.action === UserInteraction.wait ? 'WAIT' : 'BUILD TOWER');
        this._executeMove(nextMove);
      }

      return;
    }
  }

  attachToGame(game : Game) {
    this.currentGame = game;

    const bestGamePlayed = this.getBestGamePlayed();

    this._movesToExecute = bestGamePlayed.moves;

    game.addObserver(GameEventType.GameReady, () => {
      this.update(CONFIG.SIMULATION_TIMESTEP);
    });
  }

  getNextMove() : Move {
    return this._movesToExecute[0];
  }

  private _movesToExecute : Move[] = []

  private _fireObservers(eventType : UserAIEventType, eventArguments : Array<number> = []) : void {
    this._observers[eventType].forEach(observer => observer(...eventArguments));
  }
}

