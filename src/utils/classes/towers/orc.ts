import Tower, { TowerType } from '../tower';
import Monster from '../../entities/monster';

export default class OrcTower extends Tower {
  public name : string = 'orc'
  public attackCooldown : number = 300
  public damage : number = 5
  public type = TowerType.Orc
}
