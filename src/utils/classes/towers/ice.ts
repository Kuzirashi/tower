import Tower, { TowerType } from '../tower';
import Monster from '../../entities/monster';

export default class IceTower extends Tower {
  public name : string = 'ice'
  public attackCooldown : number = 500
  public damage = 70 // normally is 20
  public type = TowerType.Ice

  public upgradeCosts = {
    1: 50
  }

  public statsForLevels = {
    1: {
      damage: 70,
      attackCooldown: 500
    },
    2: {
      damage: 120,
      attackCooldown: 300
    }
  }

  public maxLevel = 2

  public dealDamageToCreature(creature : Monster) : void {
    creature.takeDamage(this.damage);
    creature.delayBetweenMovingFurther = 30;
  }
}
