import MapPosition from '../core/position';
import GameMap, { GameMapInstance } from './map';
import Monster, { MonsterEventType } from '../entities/monster';
import Tower, { TowerType, TowerEventType, getTowerCost } from './tower';
import Projectile from './projectile';
import MainLoop from '../core/mainloop';
import config from '../config/game-config';
import { getEnumValues } from '../helpers/typescript';
import IceTower from './towers/ice';
import OrcTower from './towers/orc';
import Lighting from './lighting';
import { IUserAI }  from './IUserAI';
import { GamePlay, Move, UserInteraction } from './gamePrimitives';

const MonsterStore = {
  lastId: 0,

  createMonster(pathToFollow) : Monster {
    const spawnPosition = new MapPosition(config.MAP.SPAWN_POSITION.X, config.MAP.SPAWN_POSITION.Y);

    return new Monster(spawnPosition, this.lastId++, pathToFollow);
  }
};

export enum GameEventType {
  GoldChanged,
  ScoreChanged,
  LivesChanged,
  MonsterDied,
  MonsterSpawned,
  TowerPlaced,
  ProjectilesUpdated,
  SceneReady,
  GameReady,
  End,
  RequestRender
}

export enum GameState {
  BEFORE_WAVES,
  PLAYING,
  Ended
}

const STARTING_LIVES = 10;
const STARTING_GOLD = 125;
const MONSTERS_COUNT = 145;

export default class Game {
  public map : GameMap

  public lives : number = STARTING_LIVES

  public gold : number = STARTING_GOLD

  entities : Array<Monster> = []

  towers : Array<Tower> = []

  projectiles : Array<Projectile> = []

  private _gameLoop : MainLoop = null

  public state : GameState

  public autoMakeActions : Boolean = false

  monstersToSpawn : number

  public totalGameTime : number = 0

  public lighting : Lighting

  public gameplay : GamePlay

  constructor(useLightingSystem : Boolean = false,
    autoMakeActions : Boolean = false,
    public readonly ai?: IUserAI
    ) {
    this.map = GameMapInstance;

    this._gameLoop = new MainLoop();

    this._gameLoop.setMaxAllowedFPS(10);

    this._gameLoop.setUpdate(this.updateFunction);
    this._gameLoop.setDraw(() => {
      this._fireObservers(GameEventType.RequestRender);
    });

    const eventTypes = getEnumValues(GameEventType);

    eventTypes.forEach(event => {
      this._observers[event] = [];
    });

    this.useLightingSystem = useLightingSystem;

    if (this.useLightingSystem && typeof(window) !== 'undefined') {
      this.lighting = new Lighting();
      window['lighting'] = this.lighting;
    }

    this.autoMakeActions = autoMakeActions;
  }

  public useLightingSystem : Boolean = false

  private _timeSinceSpawningLastMonster : number = 0

  public delayBetweenSpawningMonsters : number = 300

  updateFunction = (simulationTimestep : number) => {
    this.totalGameTime += simulationTimestep;

    this.entities.forEach(entity => {
      entity.update(simulationTimestep);
    });

    // each time entity position changes tower should recalculate

    this.towers.forEach(tower => {
      tower.update(simulationTimestep, this.entities, this.projectiles);
    });

    const projectilesLengthBeforeUpdatingTheirStates = this.projectiles.length;

    this.projectiles.forEach(projectile => {
      projectile.update(simulationTimestep);
    });

    const activeProjectiles = this.projectiles.filter(projectile => projectile.active);

    if (activeProjectiles.length > 0 || projectilesLengthBeforeUpdatingTheirStates !== activeProjectiles.length) {
      this.projectiles = activeProjectiles;
      this._fireObservers(GameEventType.ProjectilesUpdated);
    }

    this._timeSinceSpawningLastMonster += simulationTimestep;

    if (this._timeSinceSpawningLastMonster > this.delayBetweenSpawningMonsters) {
      if (this.monstersToSpawn > 0) {
        this.spawnMonster();
        this.monstersToSpawn--;

        this._timeSinceSpawningLastMonster = 0;
      }
    }

    if (this.autoMakeActions) {
      this.ai?.update(simulationTimestep);
    }
  };

  public startGameLoop() {
    this._gameLoop.start();
  }

  sceneReady() {
    if (this.useLightingSystem && this.lighting) {
      this.lighting.init();
    }

    this._fireObservers(GameEventType.SceneReady);
  }

  private _observers : any = {}

  public addObserver(type : GameEventType, callback) : void {
    if (!this._observers[type]) {
      throw new Error('There is no such event type to subscribe. Type: `' + type + '`');
    }

    this._observers[type].push(callback);
  }

  public removeObserver(type : GameEventType, callback) : void {
    this._observers[type] = this._observers[type].filter(observer => observer !== callback);
  }

  private _fireObservers(eventType : GameEventType, eventArguments : Array<number> = []) : void {
    this._observers[eventType].forEach(observer => {
      observer(...eventArguments)
    });
  }

  getBuildablePositions() : MapPosition[] {
    let positions : MapPosition[] = this.map.getBuildablePositions();

    this.towers.forEach(tower => {
      positions = positions.filter(position => !position.matches(tower.position))
    });

    return positions;
  }

  speedUpMonsters() {
      this.entities.forEach(monster => monster.speedInPixels = 8);
  }

  canBuildTower(type : TowerType) {
    return this.gold >= getTowerCost(type);
  }

  increaseGold(amount : number) {
    this.gold += amount;

    this._fireObservers(GameEventType.GoldChanged, [this.gold]);
  }

  decreaseGold(amount : number) {
    this.gold -= amount;

    this._fireObservers(GameEventType.GoldChanged, [this.gold]);
  }

  canUpgradeTowerAtPosition(position: MapPosition) {
    const tower = this.towers.find(t => t.position.matches(position));

    return tower?.canBeUpgraded() && tower?.playerAffordsUpgrade();
  }

  upgradeTowerAtPosition(position: MapPosition) {
    const tower = this.towers.find(t => t.position.matches(position));

    if (!tower?.canBeUpgraded()) {
      return false;
    }

    const upgradeCost = tower.getUpgradeCost();

    if (this.gold >= upgradeCost) {
      this.decreaseGold(upgradeCost);
    }

    tower.upgrade();
  }

  buildTower(position? : MapPosition, instantPlace : Boolean = false, type : TowerType = TowerType.Ice) {
    const positionToBuildNewTowerOn = position || new MapPosition(2, 2);

    let newTower : Tower;

    if (type === TowerType.Ice) {
      newTower = new IceTower(positionToBuildNewTowerOn, this);
    } else {
      newTower = new OrcTower(positionToBuildNewTowerOn, this);
    }

    this.decreaseGold(getTowerCost(type));

    const towerBuiltObserver = () => {
      const { x, y } = newTower.position.toAbsolutePixels();

      this.lighting.addLight(x, y + 16, 0.05);
    };

    if (instantPlace) {
      newTower.placeOnMap();
    }

    if (this.lighting && type === TowerType.Ice) {
      if (instantPlace) {
        towerBuiltObserver();
      } else {
        newTower.addObserver(TowerEventType.Built, towerBuiltObserver);
      }
    }

    this.towers = [
      ...this.towers,
      newTower
    ];

    this.gameplay.moves.push(
      new Move(UserInteraction.BuildTower, positionToBuildNewTowerOn, type, this.totalGameTime)
    );

    this._fireObservers(GameEventType.TowerPlaced);
  }

  giveUp() {
    this.endGame();
  }

  allMonstersSpawned() : Boolean {
    return this.monstersToSpawn === 0;
  }

  spawnMonster() {
    const newMonster = MonsterStore.createMonster(this.map.road);

    newMonster.addObserver(MonsterEventType.DEATH, () => {
      const indexOfMonster = this.entities.indexOf(newMonster);

      this.entities.splice(indexOfMonster, 1);
      this.entities = this.entities;

      this.increaseScore(1);

      this.increaseGold(1);

      this._fireObservers(GameEventType.MonsterDied);

      if (this.allMonstersSpawned && this.entities.length === 0) {
        this.endGame();
      }
    });

    newMonster.addObserver(MonsterEventType.REACHED_FINAL_DESTINATION, () => {
      const indexOfMonster = this.entities.indexOf(newMonster);

      this.entities.splice(indexOfMonster, 1);
      this.entities = this.entities;

      this.decreaseLives();

      if (this.allMonstersSpawned && this.entities.length === 0) {
        this.endGame();
      }
    });

    this.entities = [
      ...this.entities,
      newMonster
    ];

    this._fireObservers(GameEventType.MonsterSpawned);
  }

  decreaseLives() {
    this.lives--;

    this.decreaseScore(10);

    this._fireObservers(GameEventType.LivesChanged);

    if (this.lives <= 0) {
      this.endGame();
    }
  }

  public score : number = 0

  increaseScore(amount : number) {
    this.score += amount;

    this._fireObservers(GameEventType.ScoreChanged, [this.score]);
  }

  decreaseScore(amount : number) {
    this.score -= amount;

    this._fireObservers(GameEventType.ScoreChanged, [this.score]);
  }

  endGame() : void {
    if (this.state === GameState.Ended) {
      return;
    }

    this.towers = [];
    this.entities = [];
    this.projectiles = [];

    this.state = GameState.Ended;

    this.stop();

    this._fireObservers(GameEventType.End, [this.score]);
  }

  destroyTowers() {
    this.towers = [];
  }

  start() {
    if (this.autoMakeActions) {
      this.ai?.attachToGame(this);
    }

    this.prepareGame();

    console.log('START GAME!!!');

    this._gameLoop.start();
  }

  prepareGame() {
    this.lives = STARTING_LIVES;
    this.score = 0;
    this.gold = STARTING_GOLD;
    this.monstersToSpawn = MONSTERS_COUNT;
    this.totalGameTime = 0;

    this.state = GameState.PLAYING;

    this.gameplay = new GamePlay();

    this._fireObservers(GameEventType.GameReady);
  }

  stop() {
    if (this._gameLoop.isRunning()) {
      this._gameLoop.stop();
    }
  }
}

