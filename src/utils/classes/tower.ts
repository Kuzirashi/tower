import MapPosition from '../core/position';
import Monster from '../entities/monster';
import Projectile from './projectile';
import { getEnumValues } from '../helpers/typescript';
import Game from './game';

let lastId = 0;

export enum TowerType {
  Ice,
  Orc
}

export enum TowerState {
  Built,
  Construction,
  NotPlaced
}

export enum TowerEventType {
  Built,
  Placed,
  Upgraded,
  ConstructionProgress
}

const TOWER_COSTS = {
  [TowerType.Ice]: 45,
  [TowerType.Orc]: 30
};

export function getTowerCost(type : TowerType) {
  return TOWER_COSTS[type];
}

export default abstract class Tower {
  id: number
  attacked : Boolean
  position: MapPosition

  public type: TowerType

  timeFromLastAttack : number = 0

  public attackCooldown : number = 500

  private _alreadyAttacked: Boolean = false

  private _isBuilt: Boolean = false

  private _state : TowerState = TowerState.NotPlaced;

  public get isBuilt() {
    return this._isBuilt;
  }

  public range : number = 2

  public get state() : TowerState {
    return this._state;
  }

  public static getTypes() {
    return getEnumValues(TowerType);
  }

  constructor(position, public readonly game: Game) {
    this.position = position;
    this.id = lastId++;

    const eventTypes = getEnumValues(TowerEventType);

    eventTypes.forEach(event => {
      this._observers[event] = [];
    });
  }

  public name : string
  public level = 1;

  public get spriteURL() : string {
    return `public/img/towers/${this.name.toLowerCase()}/level ${this.level}/full.png`;
  }

  // For 100 simulations it took 700ms of time
  update(timeElapsed : number, monsters : Array<Monster>, projectiles : Array<Projectile>) : void {
    if (this.isBuilt) {
      this.attacked = false;
      this.timeFromLastAttack += timeElapsed;

      if (this.attackCooldown < this.timeFromLastAttack || !this._alreadyAttacked) {
        monsters.forEach(monster => {
          const absoluteXDifference = Math.abs(monster.position.x - this.position.x);
          const absoluteYDifference = Math.abs(monster.position.y - this.position.y);
          if ((absoluteXDifference <= this.range && absoluteYDifference <= this.range) && !this.attacked) {
            this._sendProjectileTowardsCreature(monster, projectiles);

            this.attacked = true;
            this.timeFromLastAttack = 0;
          }
        });

        this._alreadyAttacked = true;
      }
    } else if (this.state === TowerState.Construction) {
      this._constructionTimeDone += timeElapsed;

      if (this._constructionTimeDone >= this.constructionTime) {
        this._constructionFinished();
      } else {
        const progress = Math.floor((this._constructionTimeDone / this.constructionTime) * 100);

        this._fireObservers(TowerEventType.ConstructionProgress, [progress]);
      }
    }
  }

  private _constructionFinished() {
    this._state = TowerState.Built;
    this._isBuilt = true;

    this._fireObservers(TowerEventType.Built);
  }

  public takeMonsterHealthBeforeReaching = true

  public damage : number = 20

  public dealDamageToCreature(creature : Monster) : void {
    creature.takeDamage(this.damage);
  }

  private _sendProjectileTowardsCreature(creature : Monster, projectiles : Array<Projectile>) : void {
    const projectile : Projectile = new Projectile(this.damage, creature, this.position.getCopy(), this);

    if (this.takeMonsterHealthBeforeReaching) {
      this.dealDamageToCreature(creature);
    } else {
      projectile.addEventSubscriber('targetReached', () => {
        this.dealDamageToCreature(creature);
      });
    }

    projectiles.push(projectile);
  }

  private _constructionTimeDone = 0
  public constructionTime = 5000

  public placeOnMap() {
    this._state = TowerState.Construction;

    this._fireObservers(TowerEventType.Placed);
  }

  public upgradeCosts = {}

  public statsForLevels: { 
    [level:number]: {
      damage: number;
      attackCooldown?: number;
    }
  } = {
    1: {
      damage: 20
    }
  }

  public maxLevel = 1;

  public getUpgradeCost() : number {
    return this.upgradeCosts[this.level];
  }

  public upgrade() : boolean {
    if (!this.canBeUpgraded()) {
      return false;
    }

    const upgradeCost = this.getUpgradeCost();

    if (this.playerAffordsUpgrade()) {
      this.game.decreaseGold(upgradeCost);
    }

    this.level++;

    const newStats = this.statsForLevels[this.level];

    this.damage = newStats.damage;
    if (newStats.attackCooldown) {
      this.attackCooldown = newStats.attackCooldown;
    }

    this._fireObservers(TowerEventType.Upgraded);

    return true;
  }

  public canBeUpgraded() : boolean {
    return this.level < this.maxLevel;
  }

  public playerAffordsUpgrade() {
    const upgradeCost = this.getUpgradeCost();

    return this.game.gold >= upgradeCost;
  }

  private _observers : { [key: number]: Array<any> } = {}

  private _fireObservers(eventType : TowerEventType, eventArguments : Array<number> = []) : void {
    this._observers[eventType].forEach(observer => observer(...eventArguments));
  }

  public addObserver(type : TowerEventType, callback) : void {
    if (!this._observers[type]) {
      throw new Error('There is no such event type to subscribe. Type: `' + type + '`');
    }

    this._observers[type].push(callback);
  }
}
