import MapPosition, { GridPosition, PixelPosition } from '../core/position';
import config from '../config/game-config';
import Tower from './tower';
import Monster from '../entities/monster';

class Vector2 {
  constructor(public x, public y) {}
}

let lastId = 0;

export default class Projectile {
  events = {
    targetReached: []
  }

  public id : number

  public damage : number = 0
  public target : Monster
  public position : PixelPosition
  public active : Boolean = true
  public rotation : number = 0
  public spriteURL : string

  private _targetPosition : PixelPosition
  private _source : Tower

  constructor(damage, target : Monster, position : MapPosition, source : Tower) {
    this.damage = damage;
    this.target = target;

    this.position = position.toGridPosition().toPixelPosition().subtract(32, 32);

    this._targetPosition = target.position.toGridPosition().toPixelPosition().subtract(16, 16);

    this._source = source;

    this.spriteURL = '/public/img/towers/' + this._source.name + '/shoot.png';

    this.id = lastId++;
  }

  addEventSubscriber(eventName : string, subscriber : Function) {
    this.events[eventName].push(subscriber);
  }

  update(elapsedTime : number) : void {
    if (!this.active) {
      return;
    }

    const distanceYInPixels = this._targetPosition.y - this.position.y;
    const distanceXInPixels = this._targetPosition.x - this.position.x;

    this.rotation = Math.atan2(distanceYInPixels, distanceXInPixels);

    const movementValue = new Vector2(
      12 * Math.cos(this.rotation),
      12 * Math.sin(this.rotation)
    );

    this.position = new PixelPosition(this.position.x + movementValue.x, this.position.y + movementValue.y);

    if (Math.abs(this.position.x - this._targetPosition.x) < 5 && Math.abs(this.position.y - this._targetPosition.y) < 5) {
      this.active = false;
      this.target.takeDamage(20);
    }
  }
}
