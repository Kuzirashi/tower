import MapPosition from '../core/position';
import CONFIG from '../config/game-config';
import Pathfinding from './pathfinding';

export enum TileType {
  Road,
  None
}

export class Tile {
  constructor(public position : MapPosition, public type : TileType) { }

  get typeClass() : string {
    switch (this.type) {
      case TileType.Road:
        return 'road';
      default:
        return '';
    }
  }
}

const None = TileType.None;
const Road = TileType.Road;

export default class GameMap {
  public road : Array<MapPosition> = []

  public tiles : Array<Tile> = []

  public tilesMap : Array<Array<TileType>> = [
    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
    [None, None, None, Road, None, None, None, Road, Road, Road, Road, Road, None, None, None, Road, Road, Road, Road, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, Road, Road, Road, Road, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, None, None, None, None, None, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, None, None, None, None, None, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, None, None, None, None, None, None, None, None, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, Road, Road, Road, Road, Road, Road, Road, Road, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, None, None, None, None, None, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, None, None, None, None, None, None, None, None],
    [None, None, None, Road, None, None, None, Road, None, None, None, Road, None, None, None, None, None, None, None, None, None, None, None],
    [None, None, None, Road, Road, Road, Road, Road, None, None, None, Road, Road, Road, Road, Road, Road, Road, None, None, None, None, None],
    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, Road, None, None, None, None, None],
    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, Road, None, None, None, None, None],
    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, Road, None, None, None, None, None],
  ]

  constructor() {
    this.tilesMap.forEach((row, rowIndex) => {
      row.forEach((tileType, columnIndex) => {
        this.tiles.push(new Tile(new MapPosition(columnIndex, CONFIG.MAP.TILES_AMOUNT_Y - rowIndex - 1), tileType));
      });
    });

    const spawnPosition = new MapPosition(CONFIG.MAP.SPAWN_POSITION.X, CONFIG.MAP.SPAWN_POSITION.Y);

    const pathfinding = new Pathfinding(spawnPosition, this.tiles);

    const finalPosition = new MapPosition(CONFIG.MAP.FINAL_DESTINATION.X, CONFIG.MAP.FINAL_DESTINATION.Y);

    this.road = pathfinding.findPathTo(finalPosition);
  }

  getTileTypeOnPosition(position : MapPosition) : TileType {
    return this.tilesMap[position.y][position.x];
  }

  getBuildablePositions() : MapPosition[] {
    return this.tiles.filter(tile => tile.type === None).map(tile => tile.position);
  }

  getPositionsIntersectingRoadMap() : Array<any> {
    const positions = [];

    this.getBuildablePositions().forEach((position) => {
      let intersectingRoadPoints = 0;
      this.road.forEach(road => {
        if (position.inRange(road, 2)) {
          intersectingRoadPoints++;
        }
      });

      if (intersectingRoadPoints) {
        positions.push({
          position,
          intersectingRoadPoints
        });
      }
    });

    return positions.sort((a, b) => {
      if (b.intersectingRoadPoints > a.intersectingRoadPoints) {
        return 1;
      }

      if (b.intersectingRoadPoints < a.intersectingRoadPoints) {
        return -1;
      }

      return 0;
    });
  }
}

export const GameMapInstance = new GameMap();
