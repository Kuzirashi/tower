import { getOffsetInPixels } from '../core/position';


let LE;

if (typeof(window) !== 'undefined') {
  LE = window['LE'];
}

export default class Lighting {
  private _requestAnimationFrameHandle : number

  init() {
    // Get html canvas element
    var canvas = document.getElementById("canvas");
    // Initialise the scene
    this.scene = new LE.Scene();

    // Create lights and a polygon
    var greenLight = new LE.PointLight( { x: 605, y: 280, colour: new LE.Colour(0, 255, 0, 255) } );
    var polygon = new LE.Polygon( {
      x: 455,
      y: 255,
      vertices: LE.Vertices.square(50, 50),
      colour: new LE.Colour(50, 50, 50, 255)
    } );

    // Finally initialise the renderer with the canvas and scene
    this.renderer = new LE.WebGLRenderer( { canvas: canvas, scene: this.scene } );

    this.renderer.setClearColour(0, 0, 0, 0);

    // Start the loop
    this.update();
  }

  scene : any

  addLight(x : number, y : number, intensity : number = 0.1) {
    const light = new LE.PointLight({
      x,
      y,
      colour: new LE.Colour(0, 0, 255, 255),
      intensity
    });

    this.scene.addLight(light);
    this.update();
  }

  renderer

  update() {
    // console.log('lighting update');
    this.render();
    //this._requestAnimationFrameHandle = requestAnimationFrame(() => this.update());
  }

  clear() {
    cancelAnimationFrame(this._requestAnimationFrameHandle);
    this.scene.lights.length = 0;
    this.update();
  }

  render() {
    this.renderer.clear();
    this.renderer.render();
  }
}
