import MapPosition, { IMapPosition } from '../core/position';
import Game from './game';
import { TowerType } from './tower';

export const MAX_SINGLE_SIMULATION_TIME : number = 100000;
export const DEFAULT_MAX_SIMULATIONS = 100;

export enum SimulationPrecision {
  EXACT = 1,
  HALF = 2,
  QUARTER = 4,
  SIXTEEN = 16,
  THOUSAND = 1000
}

export enum UserInteraction {
  BuildTower,
  Wait,
  UpgradeTower
}

function flattenArray (arr) {
    return arr.reduce(function (flat, toFlatten) {
      return flat.concat(Array.isArray(toFlatten) ? flattenArray (toFlatten) : toFlatten);
    }, []);
  }
  

export interface IMove {
    action : UserInteraction
    position : IMapPosition
    towerType : TowerType
    time : number
  }
  
  export class Move implements IMove {
    constructor(
      public readonly action: UserInteraction,
      public readonly position : MapPosition,
      public readonly towerType : TowerType,
      public readonly time : number
      ) {
    }
  
    getMatrix(includeTime? : boolean, flatten? : boolean) {
      const matrix = [this.action, this.towerType, this.position.x, this.position.y];
  
      if (includeTime) {
        matrix.push(this.getSeconds());
      }
  
      if (flatten) {
        return flattenArray(matrix);
      }
  
      return matrix;
    }
  
    getSeconds() {
      return Math.round(this.time / 1000);
    }
  
    getUniqueIdentifier() {
      return this.action * 100 + this.position.x * 10 + this.position.y * 1;
    }
  
    isPossibleInGame(game : Game) {
      switch (this.action) {
        case UserInteraction.BuildTower:
          return game.canBuildTower(this.towerType);
        case UserInteraction.UpgradeTower:
          return game.canUpgradeTowerAtPosition(this.position);
        case UserInteraction.Wait:
          return true;
        default:
          throw new Error('Move not supported');
      }
    }
  
    execute(game : Game) : Boolean {
      if (!this.isPossibleInGame(game)) {
        return false;
      }
  
      if (this.action === UserInteraction.BuildTower) {
        game.buildTower(this.position, true, this.towerType);
      } else if (this.action === UserInteraction.UpgradeTower) {
        game.upgradeTowerAtPosition(this.position);
      }
  
      return true;
    }
  }

  export class GamePlay {
    public score : number
    public moves : Move[] = []
    public time : number
  
    constructor(score?: number, moves?: Move[], time?: number) {
      this.score = score;
  
      if (moves) {
        this.moves = moves;
      }
  
      this.time = time;
    }
  
    removeNotNeededMoves() {
      this.moves = this.moves.filter(move => move.action !== UserInteraction.Wait);
    }
  
    getSeconds() {
      return Math.round(this.time / 1000);
    }
  
    getHumanReadableTime() {
      return this.getSeconds() + ' seconds';
    }
  
    getMoves(amount? : number, asMatrix? : number) {
      if (typeof(amount) === 'undefined') {
        return this.moves;
      }
  
      const moves = this.moves.slice(0, amount);
  
      if (asMatrix) {
        return flattenArray(moves.map(move => move.getMatrix(true)));
      }
  
      return moves;
    }
  }