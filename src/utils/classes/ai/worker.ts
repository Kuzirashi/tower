import { expose } from "threads/worker"
import { BatchSimulatorSynchronous } from './BatchSimulator';
import { IMove } from "../gamePrimitives";

export interface IGamePlay {
    moves: IMove[];
    score: number;
    time: number;
}

const playerWorker = {
    async simulateBatchOfGames(numberOfGames: number) {
        return new Promise<{ bestGame: IGamePlay}>(resolve => {
            const simulator = new BatchSimulatorSynchronous(numberOfGames);

            simulator.start();

            const bestGame = simulator.getBestGamePlayed();

            resolve({
                bestGame: {
                    moves: bestGame.moves,
                    score: bestGame.score,
                    time: bestGame.time
                }
            });
        });
    }
}

export type TPlayerWorker = typeof playerWorker;

expose(playerWorker)