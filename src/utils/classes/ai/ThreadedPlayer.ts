import { spawn, Thread, Worker } from "threads"
import { TPlayerWorker, IGamePlay } from './worker';
import { GamePlay, Move } from "../gamePrimitives";
import MapPosition from "../../core/position";

export class ThreadedPlayer {
    history: GamePlay[] = [];

    remainingGamesToBeScheduled = 0
    gamesPlayed = 0;
    totalGamesToBePlayed = 0;

    async play(gamesToPlay: number, batchSize: number = 100): Promise<GamePlay[]> {
        this.history = [];
        this.totalGamesToBePlayed = gamesToPlay;
        this.remainingGamesToBeScheduled = gamesToPlay;
        this.gamesPlayed = 0;

        const batchGameSimulations: Promise<IGamePlay>[] = [];

        while (this.remainingGamesToBeScheduled > 0) {
            const gamesToSchedule = batchSize > this.remainingGamesToBeScheduled ? this.remainingGamesToBeScheduled : batchSize;

            batchGameSimulations.push(this._play(gamesToSchedule));

            this.remainingGamesToBeScheduled -= gamesToSchedule;
        }

        await Promise.all(batchGameSimulations);

        return this.history;
    }

    async _play(batchSize: number): Promise<IGamePlay> {
        const worker = await spawn<TPlayerWorker>(new Worker("./worker"))
        const { bestGame } = await worker.simulateBatchOfGames(batchSize)

        await Thread.terminate(worker)

        this.gamesPlayed += batchSize;

        console.log(`[ThreadedPlayer]: Played ${this.gamesPlayed} of ${this.totalGamesToBePlayed}`);

        this.history.push(new GamePlay(
            bestGame.score,
            bestGame.moves.map(m => new Move(m.action, new MapPosition(m.position.x, m.position.y), m.towerType, m.time)),
            bestGame.time
        ));

        return bestGame;
    }
}