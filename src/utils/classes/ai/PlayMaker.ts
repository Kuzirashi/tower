import Mathematics, { mathematicsInstance } from "../mathematics";
import Game, { GameEventType, GameState } from "../game";
import { getEnumValues } from "../../helpers/typescript";
import Tower, { TowerType } from "../tower";
import MapPosition from "../../core/position";
import CONFIG from '../../config/game-config';
import { Move, GamePlay, UserInteraction, SimulationPrecision } from "../gamePrimitives";

export enum PlayMakerEventType {
    Finished
}

const CONSOLE_TIME_ID = '[USER-AI][PLAYMAKER][GAME]';

export class PlayMaker {
    moves : Move[] = []
    
    public currentGame : Game
  
    private _mathematics : Mathematics
  
    private _observers = {}
  
    constructor(public readonly maxSimulationTime) {
      this._mathematics = mathematicsInstance;
  
      const eventTypes = getEnumValues(PlayMakerEventType);
  
      eventTypes.forEach(event => {
        this._observers[event] = [];
      });

      this.gameEndObserver = this.gameEndObserver.bind(this);
    }
  

    getBuildablePositions() {
      return this.currentGame.getBuildablePositions();
    }
  
    cleanup() {
      this.alreadyPlacedPositions = [];
      this.moves = [];
      this._waitTillAffordNewTower = false;
    }
  
    public precision = SimulationPrecision.EXACT

    private gameEndObserver(score: number) {
        const gameplay = new GamePlay(score, this.moves, this.currentGame.totalGameTime);
  
        gameplay.removeNotNeededMoves();
  
        // console.log(`[PLAYMAKER] [FINISHED GAME] [SCORE: ${score}] [TIME: ${gameplay.getHumanReadableTime()}]`);
  
        this.currentGame.removeObserver(GameEventType.End, this.gameEndObserver);
  
        clearTimeout(this._maxSimulationTimer);
  
        this.finishedSimulation();
    }
  
    playGame() : Game {
      this.cleanup();
    
      this.currentGame = new Game();
  
      this.currentGame.prepareGame();
  
      this.tryMakingRandomAction();

  
      this.currentGame.addObserver(GameEventType.End, this.gameEndObserver);
  
      if (this.maxSimulationTime) {
        this._maxSimulationTimer = setTimeout(() => {
          this.currentGame.giveUp();
        }, this.maxSimulationTime);
      }
  
      while (this.currentGame.totalGameTime < this.maxSimulationTime && this.currentGame.state !== GameState.Ended) {
        this.currentGame.updateFunction(CONFIG.SIMULATION_TIMESTEP);
  
        this.update(CONFIG.SIMULATION_TIMESTEP);
      }
  
      if (this.currentGame.totalGameTime > this.maxSimulationTime && this.currentGame.state !== GameState.Ended) {
        this.currentGame.giveUp();
      }

      return this.currentGame;
    }
  
    private _waitTillAffordNewTower = false
  
    public tryMakingRandomAction() {
      let availableActions = this.getAvailableMoves();
  
      const randomIndex = this._mathematics.randomIntFromInterval(0, availableActions.length - 1);
      let nextMove = availableActions[randomIndex];
      let lastMove : Move = null
  
      while (availableActions.length) {
        if (nextMove.action === UserInteraction.BuildTower) {
          if (availableActions.length === 3) {
            if (this._waitTillAffordNewTower) {
              // console.error('BUILD ICE TOWER BECAUSE WAIT TILL WAS TRUE');
              this.buildTower(TowerType.Ice);
  
              this._waitTillAffordNewTower = false;
            } else {
              this.buildTower(nextMove.towerType);
            }
          } else {
            if (availableActions.length === 2 && !this._waitTillAffordNewTower) {
              this.buildTower(nextMove.towerType);
            }
          }
        } else if (nextMove.action === UserInteraction.UpgradeTower) {
          this.upgradeTower(nextMove);
        } else if (nextMove.action === UserInteraction.Wait) {
          this.moves.push(nextMove);
        }
  
        availableActions = this.getAvailableMoves();
  
        if (nextMove.action === UserInteraction.Wait && lastMove && lastMove.action === UserInteraction.Wait) {
          // if can afford cheapest tower, but wait is randomly choosed - wait till can afford better tower
          if (availableActions.length === 2) {
            this._waitTillAffordNewTower = true;
            // console.error('SET WAIT TILL AFFORD TO TRUE');
          }
  
          break;
        }
  
        const randomIndex = this._mathematics.randomIntFromInterval(0, availableActions.length - 1);
        lastMove = nextMove;
        nextMove = availableActions[randomIndex];
      }
    }
  
    private _executeMove(moveToBeExecuted : Move) : void {
      moveToBeExecuted.execute(this.currentGame);
  
      this._movesToExecute = this._movesToExecute.filter(move => move !== moveToBeExecuted);
  
      // console.log('after: ', this._movesToExecute.length);
    }
  
    public tryMakingBestAction() {
      const nextMove = this.getNextMove();
      if (nextMove && nextMove.isPossibleInGame(this.currentGame)) {
        this._executeMove(nextMove);
      } else {
        this.tryMakingRandomAction();
      }
    }
  
    public delayBetweenThinkingOfActions : number = 200
    private _timeSinceLastThinkingOfActions : number = 0
  
    public addObserver(type : PlayMakerEventType, callback) : void {
      if (!this._observers[type]) {
        throw new Error('There is no such event type to subscribe. Type: `' + type + '`');
      }
  
      this._observers[type].push(callback);
    }
  
    finishedSimulation() : void {  
    //   console.log(`[PLAYMAKER] [FINISHED SIMULATION]`);
    //   console.timeEnd(CONSOLE_TIME_ID);
  
      this._fireObservers(PlayMakerEventType.Finished);
    }
  
    private _maxSimulationTimer
  
    getAvailableMoves() : Move[] {
      const actions : Move[] = [
        new Move(UserInteraction.Wait, null, null, this.currentGame.totalGameTime)
      ];
  
      const towerTypes : TowerType[] = Tower.getTypes();
  
      towerTypes.forEach((type : TowerType) => {
        if (this.currentGame.canBuildTower(type)) {
          const move = new Move(UserInteraction.BuildTower, null, type, this.currentGame.totalGameTime);
  
          actions.push(move);
        }
      });

      this.currentGame.towers.forEach(t => {
        if (t.canBeUpgraded() && t.playerAffordsUpgrade()) {
          const move = new Move(UserInteraction.UpgradeTower, t.position, t.type, this.currentGame.totalGameTime);
  
          // console.log('will push ');

          actions.push(move);
        }
      });
  
      return actions;
    }
  
    public positionsRoadIntersectsMap : Array<any> = null
    public bestPositionsToPlaceTowers = [];
  
    getBestPositionsToPlaceTowers() : Array<any> {
      if (this.currentGame.map && !this.positionsRoadIntersectsMap) {
        this.positionsRoadIntersectsMap = this.currentGame.map.getPositionsIntersectingRoadMap();
  
        this.bestPositionsToPlaceTowers = this.positionsRoadIntersectsMap.slice(0, 100);
      }
  
      return this.bestPositionsToPlaceTowers;
    }
  
    public alreadyPlacedPositions : MapPosition[] = []
  
    buildTower(type : TowerType) {
      const availablePositions = this.getBestPositionsToPlaceTowers().filter(positionRange => !(<MapPosition>positionRange.position).matchesOneOf(this.alreadyPlacedPositions)).map(positionRange => positionRange.position);
  
      const randomPositionIndex = this._mathematics.randomIntFromInterval(0, availablePositions.length - 1);
  
      const randomPosition = availablePositions[randomPositionIndex];
  
      this.currentGame.buildTower(randomPosition, true, type);
  
      this.alreadyPlacedPositions.push(randomPosition);
  
      this.moves.push(new Move(UserInteraction.BuildTower, randomPosition, type, this.currentGame.totalGameTime));
    }

    upgradeTower(move: Move) {
      move.execute(this.currentGame);

      this.moves.push(move);
    }
  
    public isReplaying() : Boolean {
      return Boolean(this._movesToExecute.length > 0);
    }
  
    update(timeToBeSimulated : number) : void {  
      this._timeSinceLastThinkingOfActions += timeToBeSimulated;
  
      if (this._timeSinceLastThinkingOfActions > this.delayBetweenThinkingOfActions) {
        this.tryMakingBestAction();
  
        this._timeSinceLastThinkingOfActions = 0;
      }
    }

  
    getNextMove() : Move {
      return this._movesToExecute[0];
    }
  
    private _movesToExecute : Move[] = []
  
    private _fireObservers(eventType : PlayMakerEventType, eventArguments : Array<number> = []) : void {
      this._observers[eventType].forEach(observer => observer(...eventArguments));
    }
  }