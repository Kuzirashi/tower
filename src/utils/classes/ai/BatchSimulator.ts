import { PlayMaker } from "./PlayMaker";
import { GamePlay, MAX_SINGLE_SIMULATION_TIME } from "../gamePrimitives";

export class BatchSimulatorSynchronous {
    simulationsRan : number = 0
    
    gamesHistory : GamePlay[] = []
      
    constructor(public maxSimulations = 1,
      public readonly maxSingleSimulationTime = MAX_SINGLE_SIMULATION_TIME) {  
    }
  
    start(maxSimulations? : number) {
      if (maxSimulations) {
        this.maxSimulations = maxSimulations;
      }
  
      while (this.hasAvailableSimulations()) {
          this.playGame();
      }
    }
    
    playGame() : void {
      console.debug(`[USER-AI] [PLAYING GAME] [${this.simulationsRan + 1} of ${this.maxSimulations}]`);
  
      this.simulationsRan++;
  
      const playMaker = new PlayMaker(this.maxSingleSimulationTime);
      const finishedGame = playMaker.playGame();
  
      const gameplay = new GamePlay(finishedGame.score, playMaker.moves, finishedGame.totalGameTime);
      gameplay.removeNotNeededMoves();
  
      this.gamesHistory.push(gameplay);
    }

  
    getGamesSorted() {
      return this.gamesHistory.sort((a, b) => {
        if (b.score > a.score) {
          return 1;
        }
  
        if (b.score < a.score) {
          return -1;
        }
  
        if (b.score === a.score) {
          if (b.time < a.time) {
            return 1;
          }
  
          if (b.time > a.time) {
            return -1;
          }
        }
  
        return 0;
      });
    }
  
    private _bestGamePlayed : GamePlay
  
    getBestGamePlayed() : GamePlay {
      const gamesSortedByScore = this.getGamesSorted();
  
      if (!this._bestGamePlayed) {
        this._bestGamePlayed = gamesSortedByScore[0];
      }
  
      return this._bestGamePlayed;
    }
  
    public getSerializedGamesHistory() {
      const data = {
        gamesHistory: {}
      };
  
      data.gamesHistory = this.gamesHistory;
  
      return JSON.stringify(data);
    }

    hasAvailableSimulations() : Boolean {
      return this.simulationsRan < this.maxSimulations;
    }

  }