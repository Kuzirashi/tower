const TILES_AMOUNT_X = 23;
const TILES_AMOUNT_Y = 23;
const TILE_PIXEL_SIZE = 32;

export default {
  SIMULATION_TIMESTEP: 1000 / 60,
  MAP: {
    TILES_AMOUNT_X,
    TILES_AMOUNT_Y,
    TILE_PIXEL_SIZE,
    SIZE_Y: TILES_AMOUNT_Y * TILE_PIXEL_SIZE,
    FINAL_DESTINATION: {
      X: 17,
      Y: 0
    },
    SPAWN_POSITION: {
      X: 3,
      Y: 18
    }
  }
};
