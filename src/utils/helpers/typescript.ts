export function getEnumValues(enumerable : any) : Array<number> {
  const enumValues = Object.keys(enumerable).map(k => enumerable[k]);

  return enumValues.filter(v => typeof v === 'number');
}
