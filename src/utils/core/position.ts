import CONFIG from '../config/game-config';

export enum Direction {
  Bottom,
  Left,
  Right,
  Top
}

let lastId = 0

export class GridPosition {
  public row : number
  public column : number

  constructor(column : number, row : number) {
    this.row = row;
    this.column = column;
  }

  toPixelPosition() : PixelPosition {
    return new PixelPosition(this.column * CONFIG.MAP.TILE_PIXEL_SIZE, this.row * CONFIG.MAP.TILE_PIXEL_SIZE);
  }
}

export class PixelPosition {
  public x : number
  public y : number

  public id : number

  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  subtract(x, y?) : PixelPosition {
    if (typeof(x) === 'number') {
      this.x -= x;
    }

    if (typeof(y) === 'number') {
      this.y -= y;
    }

    return this;
  }
}

export interface IMapPosition {
  x : number
  y : number
  id : number
}

export default class MapPosition implements IMapPosition {
  public id : number

  constructor(
    public x : number,
    public y : number
  ) {
    this.x = x;
    this.y = y;
    this.id = lastId++;
  }

  add(x : number, y : number) {
    this.x += x;
    this.y += y;
  }

  matches(positionToTestAgainst : MapPosition) : Boolean {
    return Boolean(this.x === positionToTestAgainst.x && this.y === positionToTestAgainst.y);
  }

  matchesOneOf(positionsToTestAgainst : MapPosition[]) : Boolean {
    for (let i = 0; i < positionsToTestAgainst.length; i++) {
      let currentPosition = positionsToTestAgainst[i];

      if (currentPosition.matches(this)) {
        return true;
      }
    }

    return false;
  }

  inRange(positionToTestAgainst : MapPosition, range : number) : Boolean {
    return Boolean(Math.abs(this.x - positionToTestAgainst.x) <= range && Math.abs(this.y - positionToTestAgainst.y) <= range);
  }

  isStraightNeighbourOf(positionToTestAgainst : MapPosition) : Boolean {
    const absoluteDeltaX = Math.abs(this.x - positionToTestAgainst.x);
    const absoluteDeltaY = Math.abs(this.y - positionToTestAgainst.y);

    return absoluteDeltaX <= 1 && absoluteDeltaY <= 1 && absoluteDeltaX !== absoluteDeltaY;
  }

  getCopy() {
    return new MapPosition(this.x, this.y);
  }

  directionTowards(position : MapPosition) : Direction {
    const deltaX = position.x - this.x;
    const deltaY = position.y - this.y;

    if (deltaX > 0 && Math.abs(deltaX) > Math.abs(deltaY)) {
      return Direction.Right;
    }

    if (deltaY < 0) {
      return Direction.Bottom;
    }

    if (deltaX < 0) {
      return Direction.Left;
    }

    return Direction.Top;
  }

  toAbsolutePixels() {
    return {
      x: this.x * CONFIG.MAP.TILE_PIXEL_SIZE,
      y: this.y * CONFIG.MAP.TILE_PIXEL_SIZE
    };
  }

  toGridPosition() : GridPosition {
    return new GridPosition(this.x + 1, CONFIG.MAP.TILES_AMOUNT_Y - this.y);
  }
}

export function getOffsetInPixels(element : HTMLElement) : any {
  let x = 0;
  let y = 0;

  while (element && !isNaN(element.offsetLeft) && !isNaN(element.offsetTop)) {
    x += element.offsetLeft - element.scrollLeft;
    y += element.offsetTop - element.scrollTop;
    element = <HTMLElement>element.offsetParent;
  }

  return {
    x,
    y
  };
}

export function getMapPixelOffset(element : HTMLElement) : any {
  let { x, y } = getOffsetInPixels(element);

  return {
    x,
    y: CONFIG.MAP.TILES_AMOUNT_Y - y
  }
}

export function directionToString(direction : Direction) : string {
  return Direction[direction];
}
