import MapPosition, { Direction, directionToString } from '../core/position';
import CONFIG from '../config/game-config';
import { getEnumValues } from '../helpers/typescript';

interface Entity {
  position: MapPosition
}

export enum MonsterEventType {
  POSITION_MARGIN_CHANGED,
  DEATH,
  REACHED_FINAL_DESTINATION
}

const HEALTH = 160;

export default class Monster implements Entity {
  public name : string = 'Creeper'
  public speedInPixels : number = 2
  public delayBetweenMovingFurther : number = CONFIG.SIMULATION_TIMESTEP

  public positionWantingToReach : MapPosition = null

  public health : number = HEALTH
  public maxHealth : number = HEALTH

  public isDead : Boolean = false

  public position : MapPosition;
  public margin : any = {
    top: 0,
    left: 0
  }
  public directionFacing : Direction = Direction.Bottom;

  public get spriteURL() : string {
    return `public/img/creatures/${this.name.toLowerCase()}/${directionToString(this.directionFacing).toLowerCase()}_${this.movementFrame}.png`;
  }

  public isMoving : Boolean = false

  private _observers = {}

  public id : number;

  constructor(position : MapPosition, id : number, pathToFollow : Array<MapPosition>) {
    this.position = position;
    this.id = id;
    this._path = pathToFollow;

    const eventTypes = getEnumValues(MonsterEventType);

    eventTypes.forEach(event => {
      this._observers[event] = [];
    });
  }

  private _changePosition(newPosition : MapPosition) : void {
    this.position = newPosition;

    this._path = this._path.filter(position => !position.matches(newPosition));

    this.positionWantingToReach = null;

    const finalPosition = new MapPosition(CONFIG.MAP.FINAL_DESTINATION.X, CONFIG.MAP.FINAL_DESTINATION.Y);

    if (newPosition.matches(finalPosition)) {
      this._fireObservers(MonsterEventType.REACHED_FINAL_DESTINATION);
    }
  }

  private _fireObservers(eventType : MonsterEventType, eventArguments : Array<any> = []) : void {
    this._observers[eventType].forEach(observer => observer(...eventArguments));
  }

  takeDamage(amount : number) {
    if (this.isDead) {
      return;
    }

    this.health -= amount;

    if (this.health <= 0) {
      this.die();
    }
  }

  die() : void {
    if (this.isDead) {
      return;
    }

    this.isDead = true;

    this._fireObservers(MonsterEventType.DEATH);
  }

  private _timeElapsedSinceLastMove : number = 0

  private _moveToDesiredPosition() {
    this._changePosition(this.positionWantingToReach);

    this.margin = {
      top: 0,
      left: 0
    };

    this._fireObservers(MonsterEventType.POSITION_MARGIN_CHANGED, [this.margin]);

    this.isMoving = false;
  }

  private _moveByMargin(type : string, amount : number) {
    this.margin[type] += amount;

    this._fireObservers(MonsterEventType.POSITION_MARGIN_CHANGED, [this.margin]);
  }

  private _path : Array<MapPosition> = []

  public moveInstantly : Boolean = false

  update(elapsedTime : number) : void {
    this.updateMovement(elapsedTime);
  }

  // For 100 simulations it took 1000ms of time
  updateMovement(elapsedTime : number) : void {
    const framesToSimulate = Math.floor(elapsedTime / CONFIG.SIMULATION_TIMESTEP);

    let simulatedFrames = 0;

    while (simulatedFrames < framesToSimulate) {
      simulatedFrames++;

      if (this.isDead) {
        return;
      }

      const nextMove = this._path[0];

      if (!this.positionWantingToReach && nextMove) {
        switch (this.position.directionTowards(nextMove)) {
          case Direction.Bottom:
            this.moveDown();
            break;
          case Direction.Right:
            this.moveRight();
            break;
          case Direction.Top:
            this.moveUp();
            break;
          case Direction.Left:
            this.moveLeft();
            break;
        }
      }

      if (!this.isMoving) {
        return;
      }

      this._timeElapsedSinceLastMove += CONFIG.SIMULATION_TIMESTEP;

      if (this.delayBetweenMovingFurther <= this._timeElapsedSinceLastMove) {
        this.handleMovementFrames(elapsedTime);

        switch (this.directionFacing) {
          case Direction.Bottom:
            if (this.margin.top < CONFIG.MAP.TILE_PIXEL_SIZE) {
              this._moveByMargin('top', this.speedInPixels);
            } else {
              this._moveToDesiredPosition();
            }
            break;
          case Direction.Top:
            if (this.margin.top > -CONFIG.MAP.TILE_PIXEL_SIZE) {
              this._moveByMargin('top', -this.speedInPixels);
            } else {
              this._moveToDesiredPosition();
            }
            break;
          case Direction.Right:
            if (this.margin.left < CONFIG.MAP.TILE_PIXEL_SIZE) {
              this._moveByMargin('left', this.speedInPixels);
            } else {
              this._moveToDesiredPosition();
            }
            break;
          case Direction.Left:
            if (this.margin.left > -CONFIG.MAP.TILE_PIXEL_SIZE) {
              this._moveByMargin('left', -this.speedInPixels);
            } else {
              this._moveToDesiredPosition();
            }
            break;
        }

        this._timeElapsedSinceLastMove = 0;
      }
    }
  }

  moveDown() : void {
    if (this.isMoving) {
      return;
    }

    this.directionFacing = Direction.Bottom;

    this.positionWantingToReach = new MapPosition(this.position.x, this.position.y - 1);

    this.isMoving = true;
  }

  moveUp() : void {
    if (this.isMoving) {
      return;
    }

    this.directionFacing = Direction.Top;

    this.positionWantingToReach = new MapPosition(this.position.x, this.position.y + 1);

    this.isMoving = true;
  }

  moveRight() : void {
    if (this.isMoving) {
      return;
    }

    this.directionFacing = Direction.Right;

    this.positionWantingToReach = new MapPosition(this.position.x + 1, this.position.y);

    this.isMoving = true;
  }

  moveLeft() : void {
    if (this.isMoving) {
      return;
    }

    this.directionFacing = Direction.Left;

    this.positionWantingToReach = new MapPosition(this.position.x - 1, this.position.y);

    this.isMoving = true;
  }

  addObserver(type : MonsterEventType, callback) : void {
    if (!this._observers[type]) {
      throw new Error('There is no such event type to subscribe. Type: `' + type + '`');
    }

    this._observers[type].push(callback);
  }

  timeFromLastChangeOfFrame : number = 0
  changeFrameSpeed : number = 100

  movementFrame : number = 0

  public handleMovementFrames(elapsedTime : number) : void {
    this.timeFromLastChangeOfFrame += elapsedTime;

    if (this.timeFromLastChangeOfFrame >= this.changeFrameSpeed) {
      if (this.movementFrame === 0 || this.movementFrame === 2) {
        this.movementFrame = 1;
      } else if (this.movementFrame === 1) {
        this.movementFrame = 2;
      }

      this.timeFromLastChangeOfFrame = 0;
    }
  }
}
