const path = require('path');
const glob = require('glob');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ThreadsPlugin = require('threads-plugin');

module.exports = () => {
  const IS_PRODUCTION = process.env.NODE_ENV === 'production';

  const entry = {
    app: './src/index.tsx',
  };

  const plugins = [
    new ThreadsPlugin(),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './index.html',
      excludeChunks: ['tests'],
    }),
    new CopyPlugin([{ from: 'public', to: 'public' }]),
  ];

  // Include tests in development builds
//   if (!IS_PRODUCTION) {
//     entry.tests = glob.sync('./tests/**/*.test.js');

//     plugins.push(
//       new HtmlWebpackPlugin({
//         filename: 'tests/index.html',
//         template: './tests/index.html',
//         inject: 'head',
//         chunks: ['tests'],
//       })
//     );
//   }

  return {
    mode: IS_PRODUCTION ? 'production' : 'development',
    entry,
    plugins,
    module: {
      rules: [
        
        {
          test: /\.tsx?$/,
          loader: 'ts-loader',
          options: {
              transpileOnly: true
          }
      },
        {
          test: /\.(scss|css)$/,
          use: [
              {
                  loader: 'style-loader' // creates style nodes from JS strings
              },
              {
                  loader: 'css-loader' // translates CSS into CommonJS
              },
              {
                  loader: 'sass-loader' // compiles Sass to CSS
              }
          ]
      },
        {
          test: /\.(png|svg|jpg|gif)$/,
          loader: 'file-loader',
          options: {
            outputPath: 'images',
          },
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.json'],
    },
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/',
    },
    devServer: {
      contentBase: path.resolve(__dirname, 'dist'),
    },
  };
};