/// <reference path="node_modules/@types/node/index.d.ts" />

import Game from './src/utils/classes/game';
import UserAI, { UserAIEventType } from './src/utils/classes/user-ai';
import * as fs from 'fs';

const amount = 400;

const ai = new UserAI(amount);

ai.addObserver(UserAIEventType.FinishedSimulations, () => {

  const dataToWrite = ai.getSerializedGamesHistory();

  const timestamp = + new Date();

  const fileName = `run-amount_${amount}-time_${timestamp}.json`;

  fs.writeFile(
    __dirname + `/research/data/${fileName}`,
    dataToWrite,
    function(err) {
      if(err) {
        return console.log(err);
      }

      console.log("The file: " + fileName +  " was saved!");
  });

  const bestGame = ai.getBestGamePlayed();

  const bestFileName = `run-score_${bestGame.score}-time_${bestGame.getSeconds()}-timestamp_${timestamp}.json`;

  fs.writeFile(
    __dirname + `/research/data/best-runs/${bestFileName}`,
    JSON.stringify(bestGame),
    function(err) {
      if(err) {
        return console.log(err);
      }

      console.log("The file: " + bestFileName +  " was saved!");
  });
});

ai.start();

console.log(__dirname);


// var fs = module(NodeModule);


// const newGame = new Game();

// newGame.start();
