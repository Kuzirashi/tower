'use strict';

module.exports = function (environment) {
  const ENV = {
    modulePrefix: 'tower-app',
    environment
  };

  return ENV;
};
